<div class="post">
	<h4 class="post-title">
		<a target="_blank" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
	</h4>
	<div class="post-excerpt">
		<?php the_excerpt() ?>
	</div>
	<div class="post-details">
		<a href="<?php the_permalink(); ?>" class="post-more">خواندن این مطلب</a>
	</div>
</div>