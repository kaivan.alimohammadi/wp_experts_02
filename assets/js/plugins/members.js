(function () {
    tinymce.PluginManager.add('members', function (editor, url) {
        // Add Button to Visual Editor Toolbar
        editor.addButton('members', {
            title: 'Members Shortcode',
            cmd: 'members',
            image: url + '/members.jpg',
        });

        editor.addCommand('members', function () {
            //var data = prompt('تعداد محصولات مورد نظر برای نمایش ؟');
            var selected_text = editor.selection.getContent({
                'format': 'html'
            });
            if (selected_text.length === 0) {
                alert('لطفا بخشی از متن را انتخاب کنید');
                return;
            }
            // var openShortCode = '[member id='+data+' ]';
            var openShortCode = '[member]';
            var closeShortCode = '[/member]';
            var return_text = '';
            return_text = openShortCode + selected_text + closeShortCode;
            editor.execCommand('mceReplaceContent', false, return_text);
            return;
        });

    });
})();