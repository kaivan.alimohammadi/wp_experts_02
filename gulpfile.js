var gulp = require('gulp');
let cleanCSS = require('gulp-clean-css');
var concat = require('gulp-concat');
gulp.task('minify', function(){
    return gulp.src('assets/css/*.css')
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(concat('all.min.css'))
        .pipe(gulp.dest('dist'));
});

// gulp.task('default',['minify-css']);