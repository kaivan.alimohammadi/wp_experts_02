<?php wp_footer(); ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.4/socket.io.js"></script>
<script>

    jQuery(document).ready(function($){
        const socket = io('http://wpexperts.local:3000',{
            query:{
                currentLocation:window.location.href
            }
        });

        $('#sample_btn').on('click',function(event){
            event.preventDefault();
            var input = $('#input_content');
            socket.emit('sendContent',{
                content:input.val()
            });

        });
        socket.on('publishMessage',function (data) {
            console.log(data);
        });
    });
</script>
</body>
</html>