<?php
define( 'PANEL_PATH', WPEXP_DIR . '/options-panel/' );
define( 'PANEL_URL', WPEXP_URL . '/options-panel/' );
define( 'PANEL_VIEWS', PANEL_PATH . 'views/' );
include "helpers.php";
loadClasses();
//callbacks
function wpx_register_options_menu() {
	add_theme_page(
		'پنل تنظیمات قالب دوره متخصص وردپرس',
		'پنل تنظیمات قالب',
		'manage_options',
		'wpx_options',
		'wpx_options_callback'

	);
}

function wpx_register_panel_assets() {
	wp_register_style( 'panel-main-style', PANEL_URL . 'assets/css/options-panel.css' );
	wp_enqueue_style( 'panel-main-style' );
}

function wpx_options_callback() {

	$currentTab = 'general';
	if ( isset( $_GET['tab'] ) && ! empty( $_GET['tab'] ) ) {
		$currentTab = $_GET['tab'];
	}
	if ( isset( $_POST['save_settings'] ) ) {
		$currentTabClass = 'WPX_'.ucfirst($currentTab);
		$currentTabClassInstance = new $currentTabClass;
		$currentTabClassInstance->save();
		//do_action( 'wpx-options-save-' . $currentTab );
	}
	$defaultHandlers = [
		'WPX_General',
		'WPX_Information',
		'WPX_Color'
	];
	$sectionHandlers = apply_filters( 'wpx_section_handlers', $defaultHandlers );
	$wpx_options     = wpx_options();
	include PANEL_VIEWS . 'index.php';

}

function wpx_general_handler() {
	$wpx_options                                               = wpx_options();
	$wpx_options['general']['wpx-options-main-sidebar-toggle'] = isset( $_POST['wpx-options-main-sidebar-toggle'] ) ? 1 : 0;
	wpx_save_options( $wpx_options );
}

function wpx_color_handler() {
	$wpx_options                                          = wpx_options();
	$wpx_options['color']['wpx-options-post-title-color'] = ! empty( $_POST['wpx-options-post-title-color'] ) ? $_POST['wpx-options-post-title-color'] : '';
	wpx_save_options( $wpx_options );
}

//hooks
add_action( 'admin_menu', 'wpx_register_options_menu' );
add_action( 'admin_enqueue_scripts', 'wpx_register_panel_assets' );
add_action( 'wpx-options-save-general', 'wpx_general_handler' );
add_action( 'wpx-options-save-color', 'wpx_color_handler' );