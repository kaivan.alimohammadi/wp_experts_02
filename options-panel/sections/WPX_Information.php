<?php
class WPX_Information extends WPX_Section_Contract
{
	public static $title = 'اطلاع رسانی';
	public static $tab = 'information';

	public function __construct() {
		parent::__construct();
		$this->viewFile = PANEL_VIEWS . 'tabs/information.php';
	}

	public function render() {
		$wpx_options= $this->wpx_options;
		include $this->viewFile;
	}

	public function save() {
		// TODO: Implement save() method.
	}

}