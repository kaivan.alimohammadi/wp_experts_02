<?php
function wpx_options(){
	return get_option('wpx-options');
}

function wpx_save_options($data){
	return update_option('wpx-options',$data);
}

function loadClasses()
{
	include "sections/WPX_Section_Contract.php";
	include "sections/WPX_General.php";
	include "sections/WPX_Information.php";
	include "sections/WPX_Colors.php";
}