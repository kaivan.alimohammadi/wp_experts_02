<div class="wrap">

    <h1>تنظیمات قالب دوره متخصص سون لرن</h1>
    <div class="wpx-options-panel-wrapper">
        <ul class="main-nav">
            <?php if($sectionHandlers && count($sectionHandlers) > 0): ?>
                <?php foreach ($sectionHandlers as $handlerClass): ?>
                    <li><a href="<?php echo add_query_arg( array( 'tab' => $handlerClass::$tab ) ); ?>"><?php echo $handlerClass::$title; ?></a></li>
                <?php endforeach; ?>
            <?php endif; ?>
        </ul>
        <div class="wpx-options-tab-content">
            <form action="" method="post">
                <?php
                $currentTabClass = 'WPX_'.ucfirst($currentTab);
                $currentTabClassObject = new $currentTabClass;
                $currentTabClassObject->render();
                ?>
				<?php //include PANEL_VIEWS . 'tabs/' . $currentTab . '.php'; ?>
            </form>
        </div>
    </div>
</div>