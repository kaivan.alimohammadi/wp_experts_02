<?php get_template_part( 'partials/store/header' ); ?>
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">فروشگاه سون لرن</a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li><a href="#">سبد خرید</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <div id="hero"></div>
    <div class="container">
        <div class="row">
			<?php if ( $products->have_posts() ): ?>
				<?php while ( $products->have_posts() ):$products->the_post(); ?>
					<?php $current_post = $products->post; ?>
                    <dov class="col-xs-12 col-md-3">
                        <div href="" class="thumbnail">
							<?php echo get_the_post_thumbnail( $current_post->ID ); ?>
                        </div>
                    </dov>
				<?php endwhile; ?>
			<?php endif; ?>
        </div>
    </div>

<?php get_template_part( 'partials/store/footer' ); ?>