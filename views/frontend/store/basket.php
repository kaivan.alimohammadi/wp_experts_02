<?php get_header(); ?>
<?php get_template_part( 'partials/top-bar' ); ?>
<?php get_template_part( 'partials/top-header' ); ?>
<?php get_template_part( 'partials/top-menu-store' ); ?>
<?php get_template_part( 'partials/components/slider' ); ?>
    <style>
        table{
            border-collapse: collapse;
        }
        td{
            border: 1px solid #ddddff;
            text-align: center;
            padding: 3px 5px;
        }
    </style>
    <div id="store-wrapper">
        <form action="/store/checkout" method="post">
            <table class="table table-bordered">
		        <?php if ( $basket_items && count( $basket_items ) > 0 ): ?>
                    <tr>
                        <th>عنوان</th>
                        <th>قیمت</th>
                        <th>تعداد</th>
                    </tr>
			        <?php foreach ( $basket_items as $item ): ?>
                        <tr>
                            <td><?php echo $item['title']; ?></td>
                            <td><?php echo $item['price'] ?></td>
                            <td><?php echo $item['count'] ?></td>
                        </tr>
			        <?php endforeach; ?>
		        <?php endif; ?>
            </table>
            <div>
                <button name="btn_checkout" >تکمیل خرید</button>
            </div>
        </form>
    </div>
<?php get_footer(); ?>