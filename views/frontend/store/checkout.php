<?php get_header(); ?>
<?php get_template_part( 'partials/top-bar' ); ?>
<?php get_template_part( 'partials/top-header' ); ?>
<?php get_template_part( 'partials/top-menu-store' ); ?>
    <style>
        table {
            border-collapse: collapse;
        }

        td {
            border: 1px solid #ddddff;
            text-align: center;
            padding: 3px 5px;
        }
    </style>
    <div id="store-wrapper">
        <form action="/store/payment" method="post">
            <div class="order_address">
                <ul>
                    <li class="item">
                        <input id="selectAddress" type="radio" name="orderAddressType" value="userAddressItem">
                        <label for="selectAddress">
                            انتخاب آدرس</label>

                    </li>
                    <li class="itemContent">
                        <div class="user_addresses">
							<?php if ( $user_addresses && count( $user_addresses ) > 0 ): ?>
                                <ul>
									<?php foreach ( $user_addresses as $address ): ?>
                                        <li>
                                            <label for="">
                                                <input type="radio" name="address" value="<?php echo $address->address_id; ?>">
												<?php echo $address->address_state . ' - ' . $address->address_city . ' - ' . $address->address_complete . ' - ' . $address->address_phone_number ?>
                                            </label>
                                        </li>
									<?php endforeach; ?>
                                </ul>
							<?php endif; ?>
                        </div>
                    </li>
                    <li class="item">
                        <input id="newAddress" type="radio" name="orderAddressType" value="userNewAddress">
                        <label for="newAddress">
                            ایجاد آدرس جدید

                        </label>

                    </li>
                    <li class="itemContent">
                        <div class="user_new_address">
                            <div class="row">
                                <input type="text" name="address_state" placeholder="استان">
                                <input type="text" name="address_city" placeholder="شهر">
                            </div>
                            <div class="row">
                                <textarea name="address_complete" cols="30" rows="10"
                                          placeholder="آدرس کامل "></textarea>
                            </div>
                            <div class="row">
                                <input type="text" name="address_phone_number" placeholder="شماره موبایل">
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="order_payment_methods box">
				<?php $payment_methods = \Application\Service\Payment\PaymentMethods::gateways(); ?>
                <ul>
					<?php foreach ( $payment_methods as $payment_method ): ?>
                        <li>
                            <input type="radio" name="paymentMethod" id="<?php echo $payment_method::$id; ?>"
                                   value="<?php echo $payment_method::$id; ?>">
                            <label for="<?php echo $payment_method::$id; ?>"><?php echo $payment_method::$title; ?></label>
                        </li>
					<?php endforeach; ?>
                </ul>
            </div>
            <div class="box">
                <input type="hidden" name="order" value="<?php echo $new_order_id; ?>">
                <button name="submitOrderPayment">پرداخت</button>
            </div>
        </form>
    </div>
<?php get_footer(); ?>