<?php get_header(); ?>
<?php get_template_part( 'partials/top-bar' ); ?>
<?php get_template_part( 'partials/top-header' ); ?>
<?php get_template_part( 'partials/top-menu-store' ); ?>
	<style>
		table {
			border-collapse: collapse;
		}

		td {
			border: 1px solid #ddddff;
			text-align: center;
			padding: 3px 5px;
		}
	</style>
	<div id="store-wrapper">
			<div class="box">
				<?php if(count($result) > 0): ?>
					<?php if($result['status']): ?>
						<div class="alert alert-success">
							<p><?php echo $result['message']; ?></p>
						</div>
	                 <? else: ?>
						<div class="alert alert-danger">
							<p><?php echo $result['message']; ?></p>
						</div>
					<?php endif; ?>
				<?php endif; ?>
			</div>
	</div>
<?php get_footer(); ?>