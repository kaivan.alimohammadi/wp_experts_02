<h1>Users</h1>
<canvas id="myChart" width="400" height="200"></canvas>
<script>
    var ctx = $("#myChart");
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: <?php echo json_encode(array_keys($users)); ?>,
            datasets: [{
                label: 'تعداد ثبت نام',
                data: <?php echo json_encode(array_values($users)); ?>,
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
	            fill:false,
                borderWidth: 1
            },
                {
                    label: 'تعداد فروش',
                    data: <?php echo json_encode(array_values($sales)); ?>,
                    backgroundColor: 'rgb(54, 162, 235)',
                    borderColor: 'rgb(32, 144, 215)',
                    fill:false,
                    borderWidth: 1
                }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });
</script>