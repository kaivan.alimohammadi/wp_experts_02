<?php
define( 'WPEXP_DIR', get_template_directory() );
define( 'WPEXP_VIEWS', WPEXP_DIR.'/views/' );
define( 'WPEXP_URL', get_template_directory_uri() );
define( 'WPEXP_LIBRARY_URL',WPEXP_URL.'/assets/library/' );
define( 'WPEXP_CSS_URL', WPEXP_URL.'/assets/css/' );
define( 'WPEXP_JS_URL', WPEXP_URL.'/assets/js/' );
define( 'WPEXP_IMAGE_URL', WPEXP_URL.'/assets/images/' );