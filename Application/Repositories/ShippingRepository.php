<?php


namespace Application\Repositories;


class ShippingRepository extends BaseRepository {

	public function __construct() {
		parent::__construct();
		$this->table = $this->prefix . 'shipping';
	}

}