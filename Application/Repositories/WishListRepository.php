<?php


namespace Application\Repositories;


class WishListRepository extends BaseRepository {
	public function __construct() {
		parent::__construct();
		$this->table = $this->prefix .'wishlist';
		$this->primary_key =='wishlist_id';
	}

	public function getUserWishList( int $user_id ) {
		return $this->db->get_results("
				SELECT product.post_title as product_title,
				product.ID as product_id
				FROM {$this->db->posts} product
				JOIN {$this->table} wishlist
				ON product.ID=wishlist.wishlist_product_id
				WHERE wishlist.wishlist_user_id={$user_id}
		");
	}
}