<?php


namespace Application\Service\Acl;


class AclService {

	public static function currentCan( string $cap ) {
		return current_user_can( $cap );
	}

	public static function addCap(int $user_id,string $cap) {
		return (new \WP_User($user_id))->add_cap($cap);
	}

	public static function addRole(int $user_id,string $role) {
		return (new \WP_User($user_id))->add_role($role);
	}

}