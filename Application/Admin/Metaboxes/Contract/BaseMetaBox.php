<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 12/18/2017
 * Time: 3:35 PM
 */

namespace Application\Admin\Metaboxes\Contract;


abstract class BaseMetaBox {
	protected $id;
	protected $title;
	protected $screens;
	protected $priority;
	protected $context;

	public function __construct() {
		add_action( 'add_meta_boxes', [ $this, 'register' ] );
		add_action('save_post',[$this,'save']);
		$this->context = 'normal';
		$this->priority = 'high';
	}

	public function register() {
		foreach ( $this->screens as $screen ) {
			add_meta_box( $this->id, $this->title, [ $this, 'render' ], $screen,$this->context,$this->priority );
		}
	}

	abstract protected function render(\WP_Post $post);
	abstract protected function save(int $post_id);
}