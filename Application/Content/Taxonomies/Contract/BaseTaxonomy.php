<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 12/20/2017
 * Time: 6:03 PM
 */

namespace Application\Content\Taxonomies\Contract;


class BaseTaxonomy {

	protected $id;
	protected $labels;
	protected $args;
	protected $types;

	public function __construct() {
		$this->args = array(
			'hierarchical'      => true,
			'labels'            => $this->labels,
			'show_ui'           => true,
			'show_admin_column' => true,
			'query_var'         => true,
			'rewrite'           => array( 'slug' => $this->id ),
		);
		//add_action( 'init', [ $this, 'register' ] );
	}

	public function register() {
		register_taxonomy( $this->id, $this->types, $this->args );
	}
}