<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 12/25/2017
 * Time: 4:50 PM
 */

namespace Application\Content;


use Application\Auth\Auth;
use Application\Utility\View;

class FormShortCode {

	public function __construct() {
		add_shortcode( 'loginForm', [ $this, 'handler' ] );
	}

	public function handler( $atts, $content = null ) {
		if ( $this->isFormPosted() ) {
			//Auth::attempt()
		}
		$content = View::render( 'frontend.contents.shortcodes.login.main' );

		return $content;
	}

	private function isFormPosted() {
		return isset( $_POST['login_submit'] );
	}

	private function getInputs() {
		return $_REQUEST;
	}

}