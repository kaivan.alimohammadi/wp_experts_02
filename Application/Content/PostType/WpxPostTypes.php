<?php

namespace Application\Content\PostType;


class WpxPostTypes {

	public static function init() {
		call_user_func( [ new ProductPostType(), 'register' ] );
		call_user_func( [ new OrderPostType(), 'register' ] );
	}

}