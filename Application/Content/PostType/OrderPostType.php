<?php
namespace Application\Content\PostType;

use Application\Content\PostType\Contract\BasePostType;

class OrderPostType extends BasePostType {

	public function __construct() {
		$this->id = 'order';
		$this->labels = array(
			'name'                  => _x( 'سفارشات', 'Post type general name', 'textdomain' ),
			'singular_name'         => _x( 'سفارش', 'Post type singular name', 'textdomain' ),
			'menu_name'             => _x( 'سفارشات', 'Admin Menu text', 'textdomain' ),
			'name_admin_bar'        => _x( 'سفارش', 'Add New on Toolbar', 'textdomain' ),
			'add_new'               => __( 'جدید', 'textdomain' ),
			'add_new_item'          => __( 'سفارش جدید', 'textdomain' ),
			'new_item'              => __( 'سفارش جدید', 'textdomain' ),
			'edit_item'             => __( 'ویرایش سفارش', 'textdomain' ),
			'view_item'             => __( 'نمایش سفارش', 'textdomain' ),
			'all_items'             => __( 'همه سفارشات', 'textdomain' ),
			'search_items'          => __( 'جستجوی سفارشات', 'textdomain' ),
			'parent_item_colon'     => __( 'سفارش والد :', 'textdomain' ),
			'not_found'             => __( 'سفارشی یافت نشد.', 'textdomain' ),
			'not_found_in_trash'    => __( 'سفارشی در زباله دان یافت نشد.', 'textdomain' ),
			'featured_image'        => _x( 'تصویر شاخص سفارش', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'textdomain' ),
			'set_featured_image'    => _x( 'تصویر سفارش را انتخاب کنید', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
			'remove_featured_image' => _x( 'حذف تصویر محسول', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
			'use_featured_image'    => _x( 'استفاده به عنوان تصویر سفارش', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
			'archives'              => _x( 'آرشیو سفارشات', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'textdomain' ),
			'insert_into_item'      => _x( 'Insert into book', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'textdomain' ),
			'uploaded_to_this_item' => _x( 'Uploaded to this book', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'textdomain' ),
			'filter_items_list'     => _x( 'Filter books list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'textdomain' ),
			'items_list_navigation' => _x( 'Books list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'textdomain' ),
			'items_list'            => _x( 'Books list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'textdomain' ),
		);
		parent::__construct();
	}

}