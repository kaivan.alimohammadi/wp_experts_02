<?php


namespace Application\Controllers\Store;


use Application\Auth\Auth;
use Application\Models\OrderStatus;
use Application\Repositories\AddressRepository;
use Application\Repositories\OrderRepository;
use Application\Repositories\ShippingRepository;
use Application\Service\Payment\PaymentMethods;
use Application\Utility\View;

class PaymentsController {

	public static function payment() {
		$address_repository = new AddressRepository();
		$order_repository   = new OrderRepository();
		$shipping_repository = new ShippingRepository();
		$result = [];
		$shipping_data      = [
			'shipping_user_id'    => Auth::current_id(),
			'shipping_order_id'   => (int) $_POST['order'],
			'shipping_address_id' => 0,
			'shipping_price'      => 0,
			'shipping_created_at' => date( 'Y-m-d H:i:s' )
		];
		if ( isset( $_POST['orderAddressType'] ) ) {
			$addressType = $_POST['orderAddressType'];
			if ( $addressType == 'userAddressItem' ) {
				$shipping_data['shipping_address_id'] = intval( $_POST['address'] );
			} else {
				$new_user_address_data = [
					'address_user_id'      => Auth::current_id(),
					'address_state'        => $_POST['address_state'],
					'address_city'         => $_POST['address_city'],
					'address_complete'     => $_POST['address_complete'],
					'address_phone_number' => $_POST['address_phone_number']
				];
				$new_user_address_id   = $address_repository->create( $new_user_address_data );
				if ( $new_user_address_id ) {
					$shipping_data['shipping_address_id'] = $new_user_address_id;
				}
			}
		}
		if ( isset( $_POST['paymentMethod'] ) ) {
			$order_id              = intval( $_POST['order'] );
			$paymentMethod         = $_POST['paymentMethod'];
			$paymentMethodClass    = PaymentMethods::getPaymentClassById( $paymentMethod );
			$paymentMethodInstance = new $paymentMethodClass( $order_id );
			$result                = $paymentMethodInstance->pay();
			if ( $result && $result['status'] ) {
				$payment_id = $result['payment_id'];
				$order_repository->update( $order_id, [
					'order_payment_id' => $payment_id,
					'order_paid_at'    => date( 'Y-m-d H:i:s' ),
					'order_status'     => OrderStatus::PAID
				] );
				$shipping_repository->create($shipping_data);
			}
		}

		View::load('frontend.store.payment',compact('result'));
	}

}