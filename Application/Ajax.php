<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 12/11/2017
 * Time: 6:43 PM
 */

namespace Application;


use Application\Auth\Auth;
use Application\Service\Basket\Basket;

class Ajax {
	public static function login() {
		$email    = $_POST['email'];
		$password = $_POST['password'];
//		$remember = $_POST['remember'];
		if ( Auth::attempt( $email, $password ) ) {
			wp_send_json( [
				'success'     => true,
				'message'     => 'شما با موفقیت در سایت لاگین شدید',
				'redirect_to' => admin_url()
			] );
		} else {
			wp_send_json( [
				'success' => false,
				'message' => 'ایمیل یا کلمه عبور اشتباه است'
			] );
		}

	}

	public static function addToBasket() {
		$product_id = intval( $_POST['product_id'] );
		if ( $product_id ) {
			$basket = new Basket();
			$result = $basket->add( $product_id );
			if ( $result ) {
				wp_send_json( [
					'success'          => true,
					'basketItemsCount' => Basket::getItemsCount()
				] );
			}
			wp_send_json( [
				'success'          => false,
				'basketItemsCount' => 0
			] );
		}
	}
}