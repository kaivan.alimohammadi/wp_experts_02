<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 12/11/2017
 * Time: 6:13 PM
 */

namespace Application\Utility;


class Url {

	public static function redirect( $url = null ) {
		if ( is_null( $url ) ) {
			wp_redirect( self::get_current_url() );
			exit;
		}
		wp_redirect( $url );
		exit;
	}

	public static function get_current_url() {
		return $actual_link = ( isset( $_SERVER['HTTPS'] ) ? "https" : "http" ) . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
	}

}