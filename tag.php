<?php get_header(); ?>
<?php get_template_part( 'partials/top-bar' ); ?>
<?php get_template_part( 'partials/top-header' ); ?>
<?php get_template_part( 'partials/top-menu' ); ?>
	<div id="wrapper">
		<div id="content-wrap">
			<?php if ( have_posts() ): ?>
				<?php while ( have_posts() ):the_post(); ?>
					<?php get_template_part( 'partials/post' ) ?>
				<?php endwhile; ?>
			<?php endif; ?>
		</div>
		<?php get_sidebar(); ?>
	</div>
<?php get_footer() ?>